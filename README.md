# Pokemon Trainer

This is a app for a pokemon-trainer to view new pokemons and add them to a personal collection from a Pokemon catalogue.

## Install
In order for the app to be executable, it was necessary to clone and install the following pakages

```
Angular:
npm install -g @angular/cli
(If not already installed)

git clone https://gitlab.com/SamuelKesete/pokemon_trainer.git
cd ng-pokemon-trainer
npm install

```

## Usage

```
create an environment folder containing a Typescript file called "environment file in "ng-pokemon-trainer" folder
the file should contain:
    export const environment = {
        production: false,
        apiUsers: <apiurl>,
        apiKey: <apikey>
    };

To start use:
ng serve
```

* Opens the App in the browser at (http://localhost:4200/).
* First textbox is for login or create user, just enter a username.
* When logged in you are able to view pokemons, see extra information by pressing "Extra info" button and add them to personal collection by clicking "Add" button.
* More pokemons can be viewed by clicking next and previous button at the bottom of the page.
* After logging in the trainer and catalogue page can be accessed by the nav-bar at the top.
* Pokemons may be removed or cleared from the trainer page by "remove" and "clear" buttons. The trainer page also has the "Extra info" button.

## Known bugs
*The index for each page(buttons between the "previous" and "next" buttons) is not acting as it is supposed when the pokemons are being fetched on initial login

## Contributors 

[Olof Johnsson](@oljojo)[Samuel Kesete](@Samuel Kesete)
