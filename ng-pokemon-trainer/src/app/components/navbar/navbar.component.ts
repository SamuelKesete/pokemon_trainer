import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private readonly userService: UserService) { }

  ifUser(): User | undefined{
   return this.userService.user
  }


  ngOnInit(): void {
  }

}
