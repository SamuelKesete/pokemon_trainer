import { Component, OnInit } from '@angular/core';
import { PokemonInfo } from 'src/app/models/pokemon-info.model';
import { PokemonListItem } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemon-catalogue-list',
  templateUrl: './pokemon-catalogue-list.component.html',
  styleUrls: ['./pokemon-catalogue-list.component.css']
})
export class PokemonCatalogueListComponent implements OnInit {
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly userService: UserService
    ) { }

  private _pageNumber: number = 0;
  private _numPages: number = 0;
  private _pokemonInfo: PokemonInfo = {} as PokemonInfo; 
  public pokeBallUrl: string = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png"

  get pokemonInfo(){
    return this._pokemonInfo;
  }

  addedPokemons(match: number): boolean{
    if(this.userService.user?.pokemon !== []){
      for(let i = 0; i < this.userService.user!.pokemon.length; i++){
        if(this.userService.user!.pokemon[i].id === match){
          return true;
        }
      }
    }
    return false
  }

  handleExtraInfo(pokemonId: number){
    this.pokemonService.fetchPokemonInfo(pokemonId)
    .subscribe({
      next: (response: PokemonInfo) =>
        this._pokemonInfo = response
    })


  }


  ngOnInit(): void {
    //this.setPokeList(this._pageNumber)
    this.pokemonService.getPokemonList()
    this._numPages = Math.ceil(this.pokemonService.pokemonList.length / 10)
    console.log("Number of pages" + this._numPages)
  }

  handleAddPokemon(pokemon: PokemonListItem): void{
    if(!this.addedPokemons(pokemon.id)){
      this.userService.storePokemonApi(pokemon)
      .subscribe({
        next: (user: User) => {
          this.userService.user = user;
        },
        error: () => {

        }
      })
    }
  }

  handlePrevious(): void{
    this._pageNumber -= 1;
    this.setPokeList(this._pageNumber)
  }
  handleNext(): void{
    this._pageNumber += 1;
    this.setPokeList(this._pageNumber)
  }

  handlePageIndex(i: number): void{
    this._pageNumber = i;
    this.setPokeList(this._pageNumber)
  }


  get pageNumber(): number {
    return this._pageNumber;
  }
  get numPages(): number {
    return this._numPages;
  }

  pageIndex(): number[]{
    console.log("Pagenumber: " + this._pageNumber)
    let pageArray = new Array
    if(this.pageNumber - 5 < 0){
      console.log("first: if")
      for(let i = 0; i < this.pageNumber; i++){
        console.log("First: " + i)
        pageArray.push(i)
      }
    }
    else{
      console.log("second: else")
      for(let i = this.pageNumber - 5; i < this.pageNumber; i++){
        console.log("Second: " + i)
        pageArray.push(i)
      }
    }
    if(this.pageNumber + 5 > this._numPages){
      console.log("third: if")
      for(let i = this.numPages; i < this.pageNumber; i++){
        console.log("Third: " + i)
        pageArray.push(i)
      }
    }
    else{
      console.log("Fourth: else")
      for(let i = this.pageNumber; i < this.pageNumber + 5; i++){
        console.log("Fourth: " + i)
        pageArray.push(i)
      }
    }
    console.log("Page array: " + pageArray)
    return pageArray;

  } 


  setPokeList(itteration: number): PokemonListItem[]{
    let tenPokemons: PokemonListItem[] = []
    for(let i = (10 * itteration) ; i < (10 * (itteration + 1)); i++){ 
      if(this.pokemonService.pokemonList[i] !== undefined){
        tenPokemons.push(this.pokemonService.pokemonList[i])
      }
    }
    return tenPokemons
  }
  

}
