import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonCatalogueListComponent } from './pokemon-catalogue-list.component';

describe('PokemonCatalogueListComponent', () => {
  let component: PokemonCatalogueListComponent;
  let fixture: ComponentFixture<PokemonCatalogueListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonCatalogueListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PokemonCatalogueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
