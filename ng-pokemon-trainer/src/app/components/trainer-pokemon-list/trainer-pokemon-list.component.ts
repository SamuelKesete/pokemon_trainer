import { Component, OnInit } from '@angular/core';
import { PokemonListItem } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { PokemonService } from 'src/app/services/pokemon.service';
import { PokemonInfo } from 'src/app/models/pokemon-info.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-trainer-pokemon-list',
  templateUrl: './trainer-pokemon-list.component.html',
  styleUrls: ['./trainer-pokemon-list.component.css']
})
export class TrainerPokemonListComponent implements OnInit {

  constructor(
    private readonly pokemonService: PokemonService,
    private readonly userService: UserService
    ) { }

  private _pageNumber: number = 0;
  private _pokemonInfo: PokemonInfo = {} as PokemonInfo; 


  pokeList(): PokemonListItem[]{
    return this.setPokeList(this._pageNumber)
  }
  get pageNumber(){
    return this._pageNumber
  }
  get pokemonInfo(){
    return this._pokemonInfo;
  }

  ngOnInit(): void {
    this.pokeList();
  }


  handleExtraInfo(pokemonId: number){
    this.pokemonService.fetchPokemonInfo(pokemonId)
    .subscribe({
      next: (response: PokemonInfo) =>
        this._pokemonInfo = response
    })


  }

  handleRemovePokemon(removePokemonId: number){
    this.userService.deletePokemonApi(removePokemonId)
    .subscribe({
      next: (user: User) => {
        this.userService.user = user;
      },
      error: () => {

      }
    })

  }

  handleClearPokemons(): void {
    //this.pokemonService.fetchPokemonInfo(1)
    this.userService.clearPokemonApi()
    .subscribe({
      next: (user: User) => {
        this.userService.user = user;
      },
      error: () => {

      }
    })
  }

  handlePrevious(): void{
    this._pageNumber -= 1;
    this.setPokeList(this._pageNumber)
  }
  handleNext(): void{
    this._pageNumber += 1;
    this.setPokeList(this._pageNumber)
  }

  setPokeList(itteration: number): PokemonListItem[]{
    let tenPokemons: PokemonListItem[] = []
    for(let i = (10 * itteration) ; i < (10 * (itteration + 1)); i++){ 
      if(this.userService.user?.pokemon[i] !== undefined){
        tenPokemons.push(this.userService.user!.pokemon[i])
      }
    }
    return tenPokemons
  }

}
