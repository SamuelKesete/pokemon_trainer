import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';


const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // DEPENDECY INJECTION
  constructor(private readonly http: HttpClient) { }

  //Models, HTTPcLIENT observaribales and RxJS OPERATORS
  public login(username: string): Observable<User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: User | undefined) => {
          if (user === undefined) {// user does not exist 
            return this.createUser(username);

          }
          return of(user);

        })
      
      )

  }

  //cheak if user exist S
  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(
        //rxjs operations
        map((response: User[]) => response.pop())
        // map the data 


      )
  }
  // if not user create a User
  // create user 
  private createUser(username: string): Observable<User> {
    // create user 
    const user = {
      username,
      pokemon: []
    };
    // we need a header 
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey,

    });
    // post- create item on a browser
    return this.http.post<User>(apiUsers, user, {
      headers
    })
  }
  // if user || create user -> store user


}








