import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { StorageKeys } from "../enums/storage-keys.enums";
import { PokemonListItem } from "../models/pokemon.model";
import { User } from "../models/user.model";
import { StorageUtil } from "../utils/storage.util";

const { apiUsers, apiKey } = environment


@Injectable({
    providedIn: 'root'
})

export class UserService {
    private _user?: User;

    get user(): User |undefined{
        return this._user;
    }
    set user(user: User| undefined){
        StorageUtil.storageSave<User>(StorageKeys.User, user!);
        this._user = user;
    }

    constructor(private readonly http: HttpClient){
        this._user = StorageUtil.storageRead(StorageKeys.User);
        
    }

    public storePokemonApi(newPokemon: PokemonListItem): Observable<User> {
        console.log("Store pokemon to api")
        const headers = new HttpHeaders({
          "Content-Type": "application/json",
          "x-api-key": apiKey
        })
        return this.http.patch<User>(`${apiUsers}/${this._user!.id}`, JSON.stringify({pokemon: [...this._user!.pokemon, newPokemon]}), {headers})
      }
      public deletePokemonApi(removePokemon: number): Observable<User> {
        const newPokemonArray = this._user!.pokemon.filter(data => data.id !== removePokemon);
        console.log(newPokemonArray)
        const headers = new HttpHeaders({
          "Content-Type": "application/json",
          "x-api-key": apiKey
        })
        return this.http.patch<User>(`${apiUsers}/${this._user!.id}`, JSON.stringify({pokemon: newPokemonArray}), {headers})
        
    
      }
    
      public clearPokemonApi(): Observable<User> {
        const headers = new HttpHeaders({
          "Content-Type": "application/json",
          "x-api-key": apiKey
        })
        return this.http.patch<User>(`${apiUsers}/${this._user!.id}`, JSON.stringify({pokemon: []}), {headers})
        
    
      }
}