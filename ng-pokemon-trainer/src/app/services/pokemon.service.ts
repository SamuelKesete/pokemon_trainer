import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { StorageKeys } from '../enums/storage-keys.enums';
import { InfoInterface, PokemonInfo } from '../models/pokemon-info.model';
import {
  PokemonList,
  PokemonListItem,
  Result,
} from '../models/pokemon.model';
import { pokemonSpeciesUrl, pokemonUrl } from '../utils/pokemon.util';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private _pokemonList: PokemonListItem[] = [];
  private _error: string = '';

  get pokemonList(): PokemonListItem[] {
    return this._pokemonList;
  }

  get error(): string {
    return this._error;
  }

  constructor(private readonly http: HttpClient) {}

  public getPokemonList() {
    let pokemons: PokemonListItem[] | undefined = StorageUtil.storageRead(StorageKeys.Pokemon);
    console.log('Pokemons ' + pokemons);
    if (pokemons !== undefined) {
      this._pokemonList = pokemons;
      console.log('No fetch');
    } else {
      this.fetchPokemonList();
      console.log('Fetch');
    }

    //if the pokemon list exists in memory
    //return  it
    //else
    //fetch and return it
  }

  private fetchPokemonList(): void{
    this.http.get<PokemonList>(pokemonUrl)
    .pipe(
      map((response: PokemonList) => {
        console.log(response)
        let tempPokemonArray: PokemonListItem[] = []
        response.results.forEach((results: Result) => {
          let id = results.url.split('/')[results.url.split('/').length - 2]
          tempPokemonArray.push({id: Number(id), name: results.name, pictureUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${Number(id)}.png`} as PokemonListItem)
        });
        return tempPokemonArray
      }),
      
    )
    .subscribe({
      next: response => {
        this._pokemonList = response;
        console.log("Pokemon list: " + this._pokemonList[1].name)
        StorageUtil.storageSave(StorageKeys.Pokemon, this._pokemonList);
      },
      error: error => console.log(error)
    });
  }
  public fetchPokemonInfo(id: number): Observable<PokemonInfo>{
    return this.http.get<InfoInterface>(pokemonSpeciesUrl + id + '/')
    .pipe(
      map((response: InfoInterface) => {
        console.log(response)
        let result: PokemonInfo = {} as PokemonInfo;
        result.pokemonName = response.name;
        result.pokemonHabitat = response.habitat.name;
        result.pokemonGeneration = response.generation.name;
        result.pokemonColor = response.color.name;
        result.pokemonGrowthRate = response.growth_rate.name;
        result.pokemonEggGroups = [];
        response.egg_groups.forEach(element => {
          result.pokemonEggGroups.push(element.name)
        })
        result.pokemonHappiness = response.base_happiness;
        return result
      })
    )
  }
}

  