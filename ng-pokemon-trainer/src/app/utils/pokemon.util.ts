export const pokemonUrl: string = `https://pokeapi.co/api/v2/pokemon?limit=10000&offset=0`
export const pokemonSpeciesUrl: string = `https://pokeapi.co/api/v2/pokemon-species/`
