
export class StorageUtil {

    public static storageSave <T> (key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value));
    }

    public static storageRead<T>(key: string): T | undefined {
        const storageValue = sessionStorage.getItem(key);
        try {
            if (storageValue) {
                return JSON.parse(storageValue) as T;
            }
            return undefined;
        }
        catch (e) {
            sessionStorage.removeItem(key);
            return undefined;
    
        }
    
    
    }

    public static addPokemonToStorage(key: string, pokemonName: string): void{
        const storedValue = sessionStorage.getItem(key);
        if(storedValue){
            const parsedStoredValue = JSON.parse(storedValue);
            parsedStoredValue.pokemon.push(pokemonName)
            sessionStorage.setItem(key, JSON.stringify(parsedStoredValue))
        }
        else{
            console.log("Couldn't find user")
            return
        }

    }

}