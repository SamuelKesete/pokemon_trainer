interface GrowthRate {
    name: string;
    url: string;
  }
  
  interface Pokedex {
    name: string;
    url: string;
  }
  
  interface PokedexNumber {
    entry_number: number;
    pokedex: Pokedex;
  }
  
  interface EggGroup {
    name: string;
    url: string;
  }
  
  interface Color {
    name: string;
    url: string;
  }
  
  interface Shape {
    name: string;
    url: string;
  }
  
  interface EvolvesFromSpecies {
    name: string;
    url: string;
  }
  
  interface EvolutionChain {
    url: string;
  }
  
  interface Generation {
    name: string;
    url: string;
  }
  
  interface Language {
    name: string;
    url: string;
  }
  
  interface Name {
    name: string;
    language: Language;
  }
  
  interface Language2 {
    name: string;
    url: string;
  }
  
  interface Version {
    name: string;
    url: string;
  }
  
  interface FlavorTextEntry {
    flavor_text: string;
    language: Language2;
    version: Version;
  }
  
  interface Language3 {
    name: string;
    url: string;
  }
  
  interface FormDescription {
    description: string;
    language: Language3;
  }
  
  interface Language4 {
    name: string;
    url: string;
  }
  
  interface Genera {
    genus: string;
    language: Language4;
  }
  
  interface Pokemon {
    name: string;
    url: string;
  }
  
  interface Variety {
    is_default: boolean;
    pokemon: Pokemon;
  }
  
  export interface InfoInterface {
    id: number;
    name: string;
    order: number;
    gender_rate: number;
    capture_rate: number;
    base_happiness: number;
    is_baby: boolean;
    is_legendary: boolean;
    is_mythical: boolean;
    hatch_counter: number;
    has_gender_differences: boolean;
    forms_switchable: boolean;
    growth_rate: GrowthRate;
    pokedex_numbers: PokedexNumber[];
    egg_groups: EggGroup[];
    color: Color;
    shape: Shape;
    evolves_from_species: EvolvesFromSpecies;
    evolution_chain: EvolutionChain;
    habitat: Habitat;
    generation: Generation;
    names: Name[];
    flavor_text_entries: FlavorTextEntry[];
    form_descriptions: FormDescription[];
    genera: Genera[];
    varieties: Variety[];
  }
  
  interface Habitat {
      name: string;
      url: string;
  }
  
  export interface PokemonInfo {
      pokemonName: string;
      pokemonHabitat: string;
      pokemonGeneration: string;
      pokemonColor: string;
      pokemonGrowthRate: string;
      pokemonEggGroups: string[];
      pokemonHappiness: number;
    }
  