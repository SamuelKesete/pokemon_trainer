export interface PokemonListItem {
    name: string,
    id: number,
    pictureUrl: string
  }
  
  export interface PokemonList {
    count: number
    next: string
    previous: string
    results: Result[]
  }
  
  export interface Result {
    name: string
    url: string
  }